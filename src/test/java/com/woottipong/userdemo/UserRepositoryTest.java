package com.woottipong.userdemo;

import com.woottipong.userdemo.jpa.BackendUserEntity;
import com.woottipong.userdemo.jpa.UserRepository;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

@Slf4j
@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class UserRepositoryTest {
    @Autowired
    UserRepository userRepository;

    @Autowired
    NamedParameterJdbcTemplate jdbc;

    @Test
    void findAllTest() {
        //act
        List<BackendUserEntity> list = userRepository.findAll();

        //assert
        assertAll(
                () -> assertNotNull(list),
                () -> assertFalse(list.isEmpty()),
                () -> list.forEach(o -> {
                    assertAll(
                            () -> assertNotNull(o),
                            () -> assertNotNull(o.getUsername()),
                            () -> assertNotNull(o.getEmail())
                    );
                })
        );
    }

    @Test
    void findByUsernameTest() {
        //arrange
        String username = "admin";

        //act
        BackendUserEntity entity = userRepository.findByUsername(username).orElse(null);


        //assert
        assertNotNull(entity);
        assertAll(
                () -> assertNotNull(entity.getUsername()),
                () -> assertNotNull(entity.getEmail()),
                () -> assertEquals("admin", entity.getUsername()),
                () -> assertEquals("admin@mail.com", entity.getEmail())
        );
    }

    @Test
    @Order(1)
    void create() {
        //arrange
        Date now = new Date();
        BackendUserEntity entity = BackendUserEntity
                .builder()
                .username("test001")
                .password(null)
                .email("test001@mail.com")
                .displayName("Test001")
                .activated(false)
                .createBy("admin")
                .createTime(now)
                .updateBy("admin")
                .updateTime(now)
                .build();

        //act
        BackendUserEntity saved = userRepository.save(entity);

        log.info("create id: {}", saved.getId());

        //assert
        assertAll(
                () -> assertNotNull(saved),
                () -> assertTrue(saved.getId() > 1L)
        );
    }

    @Test
    @Order(2)
    void update() {
        //arrange
        BackendUserEntity exist = userRepository.findByUsername("admin").orElseThrow();
        exist.setActivated(true);
        exist.setUpdateTime(new Date());

        //act
        BackendUserEntity newEntity = userRepository.save(exist);

        //assert
        assertAll(
                () -> assertNotNull(newEntity),
                () -> assertNotNull(newEntity.getId()),
                () -> assertEquals(true, newEntity.getActivated())
        );
    }

    @Test
    @Order(3)
    void delete() {
        //arrange
        BackendUserEntity exist = userRepository.findByUsername("admin").orElseThrow();
        exist.setActivated(true);
        exist.setUpdateTime(new Date());

        //act
        userRepository.delete(exist);
    }

    @Test
    @Order(10)
    public void cleanUp() {
        log.info("cleanUp()");

        //act
        Long lastId = refreshId(jdbc, "backend_user", "backend_user_id_seq");

        //assert
        log.info("last id: {}", lastId);
        assertAll(
                () -> assertTrue(lastId > 0, "last id > 0")
        );
    }

    private static <T> T refreshId(NamedParameterJdbcTemplate jdbc, String table, String keyId) {
        Map<String, Object> params = new HashMap<>();
        String sql;
        Map<String, Object> map;
        T maxId;
        sql = "select max(id) as max_id from " + table;
        map = jdbc.queryForMap(sql, params);
        //noinspection unchecked
        maxId = (T) map.get("max_id");
        sql = "select setval(:keyId, :maxId) as max_id";
        params.put("maxId", maxId);
        params.put("keyId", keyId);
        map = jdbc.queryForMap(sql, params);
        //noinspection unchecked
        maxId = (T) map.get("max_id");
        return maxId;
    }
}
