package com.woottipong.userdemo;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.woottipong.userdemo.jpa.BackendUserEntity;
import com.woottipong.userdemo.jpa.UserRepository;
import com.woottipong.userdemo.models.UserModel;
import com.woottipong.userdemo.services.UserServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.Date;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@Slf4j
@ExtendWith(MockitoExtension.class)
public class UserServiceTest {
    @InjectMocks
    private UserServiceImpl userService;
    @Mock
    private UserRepository userRepository;
    @Mock
    private ObjectMapper objectMapper;
    @Mock
    private PasswordEncoder passwordEncoder;

    @Captor
    private ArgumentCaptor<BackendUserEntity> entityCaptor;

    private BackendUserEntity mockBackendUserEntity() {
        Date now = new Date();
        BackendUserEntity entity = BackendUserEntity
                .builder()
                .username("test001")
                .password(null)
                .email("test001@mail.com")
                .displayName("Test001")
                .activated(false)
                .createBy("admin")
                .createTime(now)
                .updateBy("admin")
                .updateTime(now)
                .build();
        return entity;
    }

    private UserModel mockUserModel() {
        Date now = new Date();
        UserModel model = UserModel
                .builder()
                .id(2L)
                .username("test001")
                .email("test001@mail.com")
                .displayName("Test001")
                .activated(false)
                .createBy("admin")
                .createTime(now)
                .updateBy("admin")
                .updateTime(now)
                .build();
        return model;
    }

    @Test
    void getUserTest() {
        //arrange
        when(userRepository.findByUsername(any())).thenReturn(Optional.of(mockBackendUserEntity()));
        when(objectMapper.convertValue(any(), any(Class.class))).thenReturn(mockUserModel());
        String username = "test001";

        //act
        UserModel user = userService.getUser(username);

        //assert
        assertNotNull(user);
        assertEquals("test001", user.getUsername());
    }

    @Test
    void createUserTest() {
        //arrange
        UserModel model = mockUserModel();
        when(objectMapper.convertValue(any(UserModel.class), any(Class.class))).thenReturn(mockBackendUserEntity());
        when(objectMapper.convertValue(any(BackendUserEntity.class), any(Class.class))).thenReturn(mockUserModel());

        //act
        userService.createUser(model);

        //assert
        verify(userRepository, times(1)).save(any());
        verify(userRepository).save(entityCaptor.capture());
        BackendUserEntity entity = entityCaptor.getValue();
        assertAll(
                "entity",
                () -> assertNotNull(entity)
        );
    }

    @Test
    void updateUserTest() {
        //arrange
        UserModel model = mockUserModel();
        when(userRepository.findByUsername(any())).thenReturn(Optional.of(mockBackendUserEntity()));
        when(objectMapper.convertValue(any(BackendUserEntity.class), any(Class.class))).thenReturn(mockUserModel());

        //act
        userService.updateUser(model);

        //assert
        verify(userRepository, times(1)).save(any());
        verify(userRepository).save(entityCaptor.capture());
        BackendUserEntity entity = entityCaptor.getValue();
        assertAll(
                "entity",
                () -> assertNotNull(entity)
        );
    }

    @Test
    void deleteUserTest() {
        //arrange
        UserModel model = mockUserModel();
        when(userRepository.findByUsername(any())).thenReturn(Optional.of(mockBackendUserEntity()));
        when(objectMapper.convertValue(any(BackendUserEntity.class), any(Class.class))).thenReturn(mockUserModel());

        //act
        userService.deleteUser("test001");

        //assert
        verify(userRepository, times(1)).delete(any());
    }

    @Test
    void changePasswordTest() {
        //arrange
        when(userRepository.findByUsername(any())).thenReturn(Optional.of(mockBackendUserEntity()));

        //act
        userService.changePassword("test001", null, "test", "test");

        //assert
        verify(passwordEncoder, times(1)).encode(any());
        verify(userRepository, times(1)).save(any());
    }
}
