package com.woottipong.userdemo.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class UserModel {
    private Long id;
    private String username;
    private String email;
    private String displayName;
    private Boolean activated;
    private String createBy;
    private Date createTime;
    private String updateBy;
    private Date updateTime;
}
