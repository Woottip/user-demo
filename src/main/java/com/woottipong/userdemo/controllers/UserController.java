package com.woottipong.userdemo.controllers;

import com.woottipong.userdemo.exceptions.UserInputException;
import com.woottipong.userdemo.models.UserModel;
import com.woottipong.userdemo.services.UserService;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("/api/users")
@RestController
@AllArgsConstructor
public class UserController {

    private final UserService userService;

    @GetMapping()
    public List<UserModel> getUsers() {
        return userService.findUsers();
    }

    @GetMapping("/{username}")
    public UserModel getUser(@PathVariable("username") String username) {
        return userService.getUser(username);
    }

    @PostMapping
    public UserModel createUser(@RequestBody UserModel user) {
        return userService.createUser(user);
    }

    @PutMapping("/{username}")
    public UserModel updateUser(@PathVariable("username") String username, @RequestBody UserModel user) {
        if(!username.equals(user.getUsername())) {
            throw new UserInputException("Invalid username!");
        }
        return userService.updateUser(user);
    }

    @DeleteMapping("/{username}")
    public UserModel deleteUser(@PathVariable("username") String username) {
        return userService.deleteUser(username);
    }

    @PutMapping("/change-password")
    public void changePassword(
            @RequestParam("username") String username,
            @RequestParam(value = "currentPassword", required = false) String currentPassword,
            @RequestParam("newPassword") String newPassword,
            @RequestParam("newPasswordConfirmed") String newPasswordConfirmed) {
        userService.changePassword(username, currentPassword, newPassword, newPasswordConfirmed);
    }
}
