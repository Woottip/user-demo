package com.woottipong.userdemo.configurations;

import com.woottipong.userdemo.exceptions.ObjectNotFoundException;
import com.woottipong.userdemo.exceptions.UserInputException;
import lombok.extern.slf4j.Slf4j;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;

import java.sql.SQLException;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

@Slf4j
@RestControllerAdvice
public class ControllerExceptionHandler {
    @ExceptionHandler(value = {UserInputException.class})
    @ResponseStatus(value = HttpStatus.FORBIDDEN)
    public Map<String, Object> handleUserInputException(UserInputException ex, WebRequest request) {
        log.error("error: {}", ex.getMessage());
        return buildMessage(HttpStatus.FORBIDDEN, ex.getMessage());
    }

    @ExceptionHandler(value = {ObjectNotFoundException.class})
    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    public Map<String, Object> handleObjectNotFoundException(ObjectNotFoundException ex, WebRequest request) {
        log.error("error: {}", ex.getMessage());
        return buildMessage(HttpStatus.NOT_FOUND, ex.getMessage());
    }

    @ExceptionHandler(value = {DataIntegrityViolationException.class})
    @ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR)
    public Map<String, Object> handleObjectNotFoundException(DataIntegrityViolationException ex, WebRequest request) {
        log.error("error: {}", ex.getMessage());
        Throwable cause1 = ex.getCause();
        if(cause1 instanceof ConstraintViolationException) {
            SQLException sqlException = ((ConstraintViolationException) cause1).getSQLException();
            String sqlState = sqlException.getSQLState();
            String message = sqlException.getMessage();
            if("23502".equals(sqlState)) {
                message = message.substring(0, message.indexOf("Detail:")).trim();
            }
            return buildMessage(HttpStatus.INTERNAL_SERVER_ERROR, message);
        }
        return buildMessage(HttpStatus.INTERNAL_SERVER_ERROR, ex.getMessage());
    }

    @ExceptionHandler(value = {ConstraintViolationException.class})
    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    public Map<String, Object> handleObjectNotFoundException(ConstraintViolationException ex, WebRequest request) {
        log.error("error: {}", ex.getMessage());
        return buildMessage(HttpStatus.NOT_FOUND, ex.getMessage());
    }

    private Map<String, Object> buildMessage(HttpStatus status, String errorMessage) {
        Map<String, Object> map = new LinkedHashMap<>();
        map.put("status", status.value());
        map.put("error", status.getReasonPhrase());
        map.put("message", errorMessage);
        map.put("timestamp", new Date());
        log.error("error: {}", errorMessage);
        return map;
    }
}
