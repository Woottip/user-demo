package com.woottipong.userdemo.jpa;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserRepository extends JpaRepository<BackendUserEntity, Long> {
    Optional<BackendUserEntity> findByUsername(String username);
}
