package com.woottipong.userdemo.services;

import com.woottipong.userdemo.models.UserModel;

import java.util.List;

public interface UserService {
    List<UserModel> findUsers();

    UserModel getUser(String username);

    UserModel createUser(UserModel user);

    UserModel updateUser(UserModel user);

    UserModel deleteUser(String username);

    void changePassword(String username, String currentPassword, String newPassword, String newPasswordConfirmed);
}
