package com.woottipong.userdemo.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.woottipong.userdemo.exceptions.ObjectNotFoundException;
import com.woottipong.userdemo.exceptions.UserInputException;
import com.woottipong.userdemo.jpa.BackendUserEntity;
import com.woottipong.userdemo.jpa.UserRepository;
import com.woottipong.userdemo.models.UserModel;
import jakarta.transaction.Transactional;
import lombok.AllArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@Service
@AllArgsConstructor
public class UserServiceImpl implements UserService {

    private static final String USER_UPDATE = "admin";

    private final ObjectMapper objectMapper;

    private final PasswordEncoder passwordEncoder;

    private final UserRepository userRepository;

    @Override
    public List<UserModel> findUsers() {
        return userRepository.findAll()
                .stream()
                .map(o -> objectMapper.convertValue(o, UserModel.class))
                .sorted(Comparator.comparing(UserModel::getId))
                .toList();
    }

    @Override
    public UserModel getUser(String username) {
        return userRepository.findByUsername(username)
                .map(o -> objectMapper.convertValue(o, UserModel.class))
                .orElseThrow(() -> new ObjectNotFoundException("Username " + username + " not found!"));
    }

    @Transactional
    @Override
    public UserModel createUser(UserModel user) {
        Date now = new Date();
        BackendUserEntity entity = objectMapper.convertValue(user, BackendUserEntity.class);
        entity.setId(null);
        entity.setUsername(user.getUsername().toLowerCase());
        entity.setPassword(null);
        entity.setActivated(user.getActivated() != null && user.getActivated());
        entity.setCreateBy(USER_UPDATE);
        entity.setCreateTime(now);
        entity.setUpdateBy(USER_UPDATE);
        entity.setUpdateTime(now);
        userRepository.save(entity);
        return objectMapper.convertValue(entity, UserModel.class);
    }

    @Transactional
    @Override
    public UserModel updateUser(UserModel user) {
        String username = user.getUsername().toLowerCase();
        if("admin".equalsIgnoreCase(username)) {
            throw new UserInputException("User admin is not allow to update!");
        }
        BackendUserEntity entity = userRepository.findByUsername(username)
                .orElseThrow(() -> new ObjectNotFoundException("Username " + username + " not found!"));
        entity.setDisplayName(user.getDisplayName());
        entity.setActivated(user.getActivated());
        entity.setUpdateBy(USER_UPDATE);
        entity.setUpdateTime(new Date());
        userRepository.save(entity);
        return objectMapper.convertValue(entity, UserModel.class);
    }

    @Transactional
    @Override
    public UserModel deleteUser(String username) {
        if("admin".equalsIgnoreCase(username)) {
            throw new UserInputException("User admin is not allow to update!");
        }
        BackendUserEntity entity = userRepository.findByUsername(username)
                .orElseThrow(() -> new ObjectNotFoundException("Username " + username + " not found!"));
        UserModel model = objectMapper.convertValue(entity, UserModel.class);
        userRepository.delete(entity);
        return model;
    }

    @Transactional
    @Override
    public void changePassword(String username, String currentPassword, String newPassword, String newPasswordConfirmed) {
        if(Objects.isNull(username)) {
            throw new UserInputException("Username can not be null!");
        }
        if(Objects.isNull(newPassword) || Objects.isNull(newPasswordConfirmed)) {
            throw new UserInputException("Password can not be null!");
        }
        if(!newPassword.equals(newPasswordConfirmed)) {
            throw new UserInputException("New Password not matched!");
        }
        BackendUserEntity entity = userRepository.findByUsername(username)
                .orElseThrow(() -> new ObjectNotFoundException("Username " + username + " not found!"));
        String oldPassword = entity.getPassword();
        if(oldPassword != null) {
            if(!passwordEncoder.matches(currentPassword == null ? "" : currentPassword, oldPassword)) {
                throw new UserInputException("Current password not matched!");
            }
        }
        String encoded = passwordEncoder.encode(newPassword);
        entity.setPassword(encoded);
        entity.setUpdateTime(new Date());
        entity.setUpdateBy(USER_UPDATE);
        userRepository.save(entity);
    }
}
