CREATE TABLE backend_user (
	id bigserial NOT NULL,
	username varchar NOT NULL,
	password varchar NULL,
	email varchar NOT NULL,
	display_name varchar NOT NULL,
	activated bool NOT NULL,
	create_by varchar NOT NULL,
	create_time timestamp NOT NULL,
	update_by varchar NOT NULL,
	update_time timestamp NOT NULL,
	CONSTRAINT backend_user_pk PRIMARY KEY (id)
);
CREATE UNIQUE INDEX backend_user_email_idx ON backend_user USING btree (email);
CREATE UNIQUE INDEX backend_user_username_idx ON backend_user USING btree (username);

insert into backend_user
(username, password, email, display_name, activated, create_by, create_time, update_by, update_time)
values
('admin', null, 'admin@mail.com', 'Admin', true, 'admin', now(), 'admin', now());