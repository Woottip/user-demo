## แบบทดสอบ
package และโปรแกรมที่จะนํามาใช้งานในแบบทดสอบ
- lombok
- flyway
- mockito
- postgresql
- kafka
### ข้อที่ 1
- ให้สร้างโปรเจค Spring boot โดยใช้ spring initializr ต้องเลือก package ที่ระบุไว้ด้านบนและสามารถลง package อื่นๆ ได้
  หลังจากสร้างเสร็จให้ทำการ push code ไปที่ gitlab ตามลิงค์ที่แนบไว้ด้านบน ผลลัพธ์ที่คาดหวัง สามารถ run project spring-boot รวมถึงสามารถใช้งาน git ได้
- คำตอบที่ส่ง:
```
 git clone https://gitlab.com/Woottip/user-demo.git
```
เมื่อ clone เสร็จแล้วให้รันไฟล์ docker-compose.yml เพื่อ start postgres และรัน java class UserDemoApplication เพื่อ start application

### ข้อที่ 2
- ให้สร้าง Controller file พร้อมทั้ง return ข้อความออกทางหน้าจอว่า Hello Backend Team
- คำตอบที่ส่ง: เมื่อ start application แล้ว ให้เข้าไปที่ url http://localhost:8082/api/hello ผ่าน browser หรือ postman

### ข้อที่ 3
- ให้ทำการรัน docker-compose ไฟล์ของ postgrest sql เพื่อทดสอบการเชื่อมต่อกับฐานข้อมูล โดยเมื่อสามารถเชื่อมต่อได้แล้วให้ทำการสร้างฐานข้อมูลชื่อ scv_backend_team
- คำตอบที่ส่ง: (ใช้คำตอบ ข้อที่ 1)

### ข้อที่ 4
- ให้ทำการเชื่อมต่อ spring-boot application เข้ากับ database postgres sql กับฐานข้อมูล scv_backend_team
- คำตอบที่ส่ง: (ใช้คำตอบ ข้อที่ 1)

### ข้อที่ 5
- ให้สร้าง Flyway migration file โดยให้ Flyway ทำหน้าที่สร้าง table user ภายใต้ฐานข้อมูล scv_backend_team ในส่วนชื่อ field สามารถออกแบบได้ตามจินตนาการ
- คำตอบที่ส่ง: migration file อยู่ที่ path: ./src/main/resources/db/migration/

### ข้อที่ 6
- ให้สร้าง File Service, Repository, Service, Mapper, Model ขึ้นมาภายในโปรเจคและเชื่อมโยงแต่ละไฟล์เข้าด้วยกันโดยให้โปรแกรมสามารถทำงานได้
- คำตอบที่ส่ง:
- สามารถใช้งาน Restful API ตาม UserController โดยมี API ดังนี้
  - http://localhost:8082/api/users GET : get all users
  - http://localhost:8082/api/users/{username} GET : get user by username
  - http://localhost:8082/api/users POST : create user
  - http://localhost:8082/api/users/{username} PUT : update user
  - http://localhost:8082/api/users/{username} DELETE : delete user
  - http://localhost:8082/api/users/change-password?username={...}&currentPassword={...}&newPassword={...}&newPasswordConfirmed={...} PUT: change password
- สามารถใช้งาน jUnit Test ที่ path: ./src/test